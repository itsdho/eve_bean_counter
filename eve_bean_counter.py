﻿#! /usr/bin/env python

"""Asks for your EVE API key, and gets your wallet journal and wallet transactions.

Author: David Ho
Last modified: 2013 Apr 10
"""

import os
import os.path
import errno
import re
import urllib.request
import urllib.parse
import xml.etree.ElementTree as ET
import csv
import sqlite3
from contextlib import closing

CHARACTERS_URL = 'https://api.eveonline.com/account/characters.xml.aspx'
WALLET_JOURNAL_URL = 'https://api.eveonline.com/char/WalletJournal.csv.aspx'
WALLET_TRANSACTIONS_URL = 'https://api.eveonline.com/char/WalletTransactions.csv.aspx'
MAX_ROW_COUNT = 2560

WALLET_JOURNAL_HEADER = ['date', 'refID', 'refType', 'ownerName1', 'ownerName2', 'argName1', 'amount', 'balance', 'reason', 'taxReceiverID', 'taxAmount']
WALLET_TRANSACTIONS_HEADER = ['date', 'transID', 'quantity', 'type', 'price', 'clientName', 'stationName', 'transactionType', 'transactionFor', 'journalTransactionID']

def get_character_info(character_name, api_info_fname='api_info.txt'):
    """Reads the character's API info (EVE API key, verification code, and character id) from an existing text file. If the file doesn't exist, asks the user for the info, and saves it in the text file.
    
    Arguments:
      - character_name [string]: The character's name.
      - api_info_fname [string]: The name of the text file where the API info is stored.
    
    Returns:
      - keyID [int]: The EVE API key ID.
      - vCode [string]: The EVE API verification code.
      - characterID [int]: The EVE API character ID.
    """
    
    api_info_path = os.path.join(character_name, api_info_fname)
    if os.path.isdir(character_name) and os.path.isfile(api_info_path):
        with open(api_info_path, 'r') as f:
            lines = f.readlines()
        raw_keyID = lines[1]
        keyID = re.sub(r'[^0-9]+', '', raw_keyID)
        raw_vCode = lines[2]
        vCode = re.sub(r'[^a-zA-Z0-9]+', '', raw_vCode)
        raw_characterID = lines[3]
        characterID = re.sub(r'[^0-9]+', '', raw_characterID)
    else:
        os.makedirs(character_name, exist_ok=True)
        raw_keyID = input('Paste your EVE API "Key ID":\n')
        keyID = re.sub(r'[^0-9]+', '', raw_keyID)
        raw_vCode = input('Paste your EVE API "Verification Code":\n')
        vCode = re.sub(r'[^a-zA-Z0-9]+', '', raw_vCode)
        
        data = urllib.parse.urlencode({'keyID':keyID, 'vCode':vCode}).encode('utf-8')
        with closing(urllib.request.urlopen(CHARACTERS_URL, data)) as response: # POST request
            tree = ET.parse(response)
        
        character_node = tree.getroot().findall(".//rowset[@name='characters']/row[@name='{}']".format(character_name))[0]
        characterID = character_node.get('characterID')
        
        lines = [character_name, keyID, vCode, characterID]
        with open(api_info_path, 'w') as f:
            f.writelines('\n'.join(lines))
    
    return keyID, vCode, characterID

def download_csv(keyID, vCode, characterID, kind, fname=None, rowCount=MAX_ROW_COUNT, fromID=None):
    """Retrieves the latest entries (up to 2560) in the character's wallet journal or wallet transactions, using the EVE API.
    
    Arguments:
        - keyID [int]: The EVE API key ID.
        - vCode [string]: The EVE API verification code.
        - characterID [int]: The EVE API character ID.
        - kind [string]: This must be either 'wallet journal' or 'wallet transactions'.
        - fname [string]: The filename where we should save the retrieved csv file.
        - rowCount [int]: The number of rows to retrieve.
        - fromID [int]: The refID or transID to start with. If this is None, just retrieve the latest entries.
    
    Returns:
        - num_rows_retrieved [int]: The number of rows retrieved (not counting the header).
    """
    
    if kind == 'wallet journal':
        url = WALLET_JOURNAL_URL
        num_fields = len(WALLET_JOURNAL_HEADER)
        if not fname:
            fname = 'latest_wallet_journal.csv'
    elif kind == 'wallet transactions':
        url = WALLET_TRANSACTIONS_URL
        num_fields = len(WALLET_TRANSACTIONS_HEADER)
        if not fname:
            fname = 'latest_wallet_transactions.csv'
    else:
        raise RuntimeError('kind must be either \'wallet journal\' or \'wallet transactions\'')
    
    data = {}
    for k in ['keyID', 'vCode', 'characterID', 'rowCount', 'fromID']:
        data[k] = locals()[k] # store variables in a dict, with their names as keys. hacky! ^_^
    data = urllib.parse.urlencode(data).encode('utf-8')
    
    print('downloading {} starting from {}'.format(kind, 'latest' if not fromID else fromID))
    with closing(urllib.request.urlopen(url, data)) as response: # POST request
        raw_text = response.read().decode('utf-8') # one giant string
    
    # FIXME: whenever you have a 'DESC:' entry, which apparently ends in a \n, response.readlines() breaks it into 2 separate lines. 
    # furthermore, csv.reader doesn't let us specify a line terminator; otherwise we could choose to terminate at '\r\n' instead of 'all types of newlines'.
    # as a workaround, we're using read(), and then re.sub(). there might be a more elegant way to do this, though?
    lines = re.sub(r'DESC:(.*?)\n,', r'DESC:\1,', raw_text).split('\r\n') #TODO: do lines end in \r\n on windows too?
    rows = [row for row in csv.reader(lines, quoting=csv.QUOTE_MINIMAL) if row] # remove any empty rows
    
    for row in rows: # rows is a list of list of strings
        if len(row) != num_fields:
            raise RuntimeError('this row has {} fields, when it should have {}!\n{}'.format(len(rows[-1]), num_fields, rows[-1]))
    
    num_rows_retrieved = len(rows) - 1
    print('    {} rows retrieved'.format(num_rows_retrieved))
    
    print('    writing to {}'.format(fname))
    with open(fname, 'w', newline='') as f:
        csv.writer(f, quoting=csv.QUOTE_MINIMAL).writerows(rows)
    
    return num_rows_retrieved

def determine_kind(fname):
    """Determines whether a csv file looks like a wallet journal or a wallet transactions file, based on its headers.
    
    Arguments:
        - fname [string]: The filename of a wallet journal or wallet transactions csv file.
    
    Returns:
        - kind [string]: Either 'wallet journal' or 'wallet transactions'.
    """
    
    with open(fname, 'r', newline='') as f:
        rows = csv.reader(f, quoting=csv.QUOTE_MINIMAL)
        header = next(rows) # this returns a list of the column names in the first line of the csv file
    
    if header == WALLET_JOURNAL_HEADER:
        kind = 'wallet journal'
    elif header == WALLET_TRANSACTIONS_HEADER:
        kind = 'wallet transactions'
    else:
        kind = None
        raise RuntimeError('unrecognized header:\n{}'.format(header))
    
    return kind

def insert_from_csv(csv_fname, cur):
    """Inserts entries from a wallet journal or wallet transaction csv file into the database (ignoring duplicate entries).
    
    Arguments:
        - csv_fname [string]: The filename of the csv file to import into the database.
        - cur [sqlite3.Cursor]: A cursor into the sqlite3 database where we're storing the wallet journal and wallet transactions.
    
    Returns:
        - num_rows_read [int]: The number of rows read from the csv file, not counting the header.
        - num_rows_inserted [int]: The number of rows inserted into the database. NOTE: this only counts the rows that were actually modified, NOT counting the ones that are ignored.
    """
    
    print('importing from {}'.format(csv_fname))
    kind = determine_kind(csv_fname)
    print('    {} header read'.format(kind))
    
    if kind == 'wallet journal':
        insert_statement = 'INSERT OR IGNORE INTO wallet_journal (date, refID, refType, ownerName1, ownerName2, argname1, amount, balance, reason, taxReceiverID, taxAmount) \
                                      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);'
    elif kind == 'wallet transactions':
        insert_statement = 'INSERT OR IGNORE INTO wallet_transactions (date, transID, quantity, type, price, clientName, stationName, transactionType, transactionFor, journalTransactionID) \
                                                  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);'
    else:
        raise RuntimeError('kind must be either \'wallet journal\' or \'wallet transactions\'')
    
    with open(csv_fname, 'r', newline='') as f:
        rows = list(csv.reader(f, quoting=csv.QUOTE_MINIMAL))[1:]
    num_rows_read = len(rows)
    print('    {} rows read'.format(num_rows_read))
    
    cur.executemany(insert_statement, rows)
    num_rows_inserted = max(0, cur.rowcount)
    cur.connection.commit()
    print('    {} rows inserted into {} table'.format(num_rows_inserted, kind))
    
    return num_rows_read, num_rows_inserted

def export_csvs(cur, wj_fname='full_wallet_journal.csv', wt_fname='full_wallet_transactions.csv'):
    """Exports all entries from the wallet journal, and/or the wallet transactions table, to csv files.
    
    Arguments:
        - cur [sqlite3.Cursor]: A cursor into the sqlite3 database where we're storing the wallet journal and wallet transactions.
        - wj_fname [string]: The filename where we should save the wallet journal csv file. If this is None, we won't export a wallet journal file.
        - wt_fname [string]: The filename where we should save the wallet transactions csv file. If this is None, we won't export a wallet transactions file.
    
    Returns:
        - num_wj_rows [int]: The number of rows in the wallet journal file, not counting the header. If we're not exporting a wallet journal file, this is None.
        - num_wt_rows [int]: The number of rows in the wallet transactions file, not counting the header. If we're not exporting a wallet journal file, this is None.
    """
    
    num_wj_rows = None
    num_wt_rows = None
    
    if wj_fname:
        cur.execute('SELECT * FROM wallet_journal ORDER BY date DESC, refID DESC')
        wj_rows = cur.fetchall()
        num_wj_rows = len(wj_rows)
        print('writing {} rows to {}'.format(num_wj_rows, wj_fname))
        with open(wj_fname, 'w', newline='') as f:
            csv.writer(f, quoting=csv.QUOTE_MINIMAL).writerow(WALLET_JOURNAL_HEADER)
            csv.writer(f, quoting=csv.QUOTE_MINIMAL).writerows(wj_rows)
    if wt_fname:
        cur.execute('SELECT * FROM wallet_transactions ORDER BY date DESC, transID DESC')
        wt_rows = cur.fetchall()
        num_wt_rows = len(wt_rows)
        print('writing {} rows to {}'.format(num_wt_rows, wt_fname))
        with open(wt_fname, 'w', newline='') as f:
            csv.writer(f, quoting=csv.QUOTE_MINIMAL).writerow(WALLET_TRANSACTIONS_HEADER)
            csv.writer(f, quoting=csv.QUOTE_MINIMAL).writerows(wt_rows)
    
    return num_wj_rows, num_wt_rows

def walk_and_insert_all(keyID, vCode, characterID, character_name, kind, cur, delete_temp_files=True):
    """Requests the latest wallet journal or wallet transactions, then repeatedly finds the lowest refID or transID, and starts the next request from there, until the EVE API servers won't give us any more. This will apparently give us up to 1 month of data?
    
    See http://wiki.eve-id.net/APIv2_Char_JournalEntries_XML#Journal_Walking.
    
    Arguments:
        - keyID [int]: The EVE API key ID.
        - vCode [string]: The EVE API verification code.
        - characterID [int]: The EVE API character ID.
        - character_name [string]: The character's name.
        - kind [string]: This must be either 'wallet journal' or 'wallet transactions'.
        - cur [sqlite3.Cursor]: A cursor into the sqlite3 database where we're storing the wallet journal and wallet transactions.
        - delete_temp_files [bool]: Whether to delete all the temporary csv files generated when walking backwards.
    """
    
    if kind == 'wallet journal':
        fname = os.path.join(character_name, 'latest_wallet_journal.csv')
        table_name = 'wallet_journal'
        primary_key = 'refID'
    elif kind == 'wallet transactions':
        fname = os.path.join(character_name, 'latest_wallet_transactions.csv')
        table_name = 'wallet_transactions'
        primary_key = 'transID'
    else:
        raise RuntimeError('kind must be either \'wallet journal\' or \'wallet transactions\'')
    
    fromID = None
    fnames = []
    while True:
        fname = os.path.join(character_name, '{}_from_{}.csv'.format(table_name, fromID if fromID else 'latest'))
        num_rows_retrieved = download_csv(keyID, vCode, characterID, kind, fname, MAX_ROW_COUNT, fromID)
        fnames.append(fname)
        num_rows_read, num_rows_inserted = insert_from_csv(fname, cur)
        
        if num_rows_retrieved < MAX_ROW_COUNT or num_rows_inserted == 0:
            break
        else:
            cur.execute('SELECT MIN({}) FROM {}'.format(primary_key, table_name))
            fromID = cur.fetchone()[0] # cur.fetchone() returns a tuple
    
    if delete_temp_files:
        for fname in fnames:
            print('deleting {}'.format(fname))
            os.remove(fname)
    
    return

def ensure_tables_exist(cur):
    """Creates wallet_journal and wallet_transactions tables if they don't exist.
    
    Arguments:
        - cur [sqlite3.Cursor]: A cursor into the sqlite3 database.
    """
    cur.execute('CREATE TABLE IF NOT EXISTS wallet_journal \
    (date TEXT, \
    refID INTEGER PRIMARY KEY, \
    refType TEXT, \
    ownerName1 TEXT, \
    ownerName2 TEXT, \
    argName1 TEXT, \
    amount REAL, \
    balance REAL, \
    reason TEXT, \
    taxReceiverID INT, \
    taxAmount REAL);')

    cur.execute('CREATE TABLE IF NOT EXISTS wallet_transactions \
    (date TEXT, \
    transID INTEGER PRIMARY KEY, \
    quantity INTEGER, \
    type TEXT, \
    price REAL, \
    clientName TEXT, \
    stationName TEXT, \
    transactionType TEXT, \
    transactionFor TEXT, \
    journalTransactionID INTEGER);')
    
    ## TODO: we should also ensure that the tables have the right schema?
    # be careful of http://en.wikipedia.org/wiki/Time_of_check_to_time_of_use
    
    cur.connection.commit()
    
    return

def main():
    ## check for a default character
    default_character_name = ''
    try:
        with open('default_character.txt', 'r') as f:
            raw_default_character_name = f.readline()
            default_character_name = re.sub(r'[^a-zA-Z0-9 ]+', '', raw_default_character_name) # sanitize strings from files
            prompt = 'What is your character\'s name (press ENTER for default [{}])?\n'.format(default_character_name)
    except IOError as e:
        if e.errno == errno.ENOENT: # if there's no default character, simply ask for a character later
            prompt = 'What is your character\'s name?\n'
        else:
            raise
    
    ## ask for character name, and get that character's api info
    raw_character_name = input(prompt)
    character_name = re.sub(r'[^a-zA-Z0-9 ]+', '', raw_character_name) # sanitize strings from users
    if not character_name:
        character_name = default_character_name
    keyID, vCode, characterID = get_character_info(character_name)
    
    ## open the database for that character (creating it if it doesn't exist)
    db_fname=os.path.join(character_name, 'wallet_journal_and_transactions.db')
    
    with closing(sqlite3.connect(db_fname)) as conn:
        with closing(conn.cursor()) as cur:
            
            ## create tables if they don't exist; display number of rows in each
            ensure_tables_exist(cur)
            cur.execute('SELECT COUNT(*) FROM wallet_journal')
            num_wj_rows = max(0, cur.fetchone()[0])
            print('found {} rows in wallet_journal'.format(num_wj_rows))
            cur.execute('SELECT COUNT(*) FROM wallet_transactions')
            num_wt_rows = max(0, cur.fetchone()[0])
            print('found {} rows in wallet_transactions'.format(num_wt_rows))
            
            ## walk backwards through entries
            walk_and_insert_all(keyID, vCode, characterID, character_name, 'wallet journal', cur)
            walk_and_insert_all(keyID, vCode, characterID, character_name, 'wallet transactions', cur)
    
            ## export the database to two csv files
            full_wj_fname = os.path.join(character_name, 'full_wallet_journal.csv')
            full_wt_fname = os.path.join(character_name, 'full_wallet_transactions.csv')
            export_csvs(cur, full_wj_fname, full_wt_fname)
    
    ## save the most recently-used character name as a default for next time
    with open('default_character.txt', 'w') as f:
        f.writelines((character_name))
    
    return

if __name__ == '__main__':
    main()
